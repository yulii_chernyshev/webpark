Summary
Solution constist of 2 projects:

ParkLibrary - .NET Standard library application which contain a parking logic
Parkovka2 - ASP.NET Core WebAPI application which provide methods for working with parking


REST API:
Vehicles:
GetCars - GET: api/vehicles
GetCar - GET: api/vehicles/number
AddCar - GET: api/vehicles
DeleteCar - DELETE : api/cars/{number}

Parking:
FreePlaces - GET : api/parking/FreePlaces
OccupiedPlaces - GET : api/parking/OccupiedPlaces
Balance - GET : api/parking/Balance

Transactions:
GetTransactionLog - GET : api/transactions/TransactionLog/
GetTransactions - GET : api/transactions/Transactions/
GetCarTransactions - GET : api/transactions/CarTransactions/{number}
AddCarBalance - PUT: api/transactions/AddCarBalance/{number}

POST and GET body:
body POST for AddCar
{
	"VehicleId" : "test3",
	"balance" : 1260,
	"vehicleType" : 3
}


vehicleType:

Passenger -0
Truck - 1
Bus - 2
Motorcycle - 3

body PUT for AddCarBalance
{
	"value":1000
}