﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkLibrary;

namespace Parkovka2.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    //[ApiController]
    public class VehiclesController : Controller
    {
        // GET : api/Vehicles
        [HttpGet]
        public JsonResult GetCars()
        {
            return Json(Parking.Instance.vehicles.ToList<Vehicle>());
        }

        // GET : api/Vehicles/number
        [HttpGet("{number}")]
        public JsonResult GetCar(string number)
        {
            Vehicle vehicle = Parking.Instance.vehicles.Find((v) => { return v.VehicleNum == number; });
            if (vehicle == null)
                HttpContext.Response.StatusCode = 404;
            return Json(vehicle);
        }

        // POST : api/cars
        [HttpPost]
        public JsonResult AddCar([FromBody]Vehicle vehicle)
        {
            try
            {
                vehicle.AddToParking(Parking.Instance);
                HttpContext.Response.StatusCode = 201;  //Добавлено 
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = 406; //Неприемлимо
                return Json(ex.Message);
            }
            return Json(vehicle);
        }

        // DELETE : api/cars/{number}
        [HttpDelete("{number}")]
        public void DeleteCar(string number)
        {
            Vehicle vehicle = Parking.Instance.vehicles.Find((v) => { return v.VehicleNum == number; });
            if (vehicle == null)
            {
                HttpContext.Response.StatusCode = 404;
                return;
            }
            try
            {
                vehicle.RemoveFromParking(Parking.Instance);
            }
            catch (System.Exception)
            {
                HttpContext.Response.StatusCode = 402;  //Необходима оплата
                return;
            }
            HttpContext.Response.StatusCode = 204;
        }

    }
}