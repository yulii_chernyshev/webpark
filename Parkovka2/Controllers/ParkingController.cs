﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkLibrary;

namespace Parkovka2.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[ApiController]
    public class ParkingController : Controller
    {


        // GET : api/Parking/FreePlaces
        [HttpGet]
        public JsonResult FreePlaces()
        {
            var free = new { freePlaces = Parking.Instance.vehicles.Capacity - Parking.Instance.vehicles.Count };
            return Json(free);
        }


        // GET : api/parking/OccupiedPlaces
        [HttpGet]
        public JsonResult OccupiedPlaces()
        {
            var occupiedPlaces = new { occupiedPlaces = Parking.Instance.vehicles.Count };
            return Json(occupiedPlaces);
        }

        // GET : api/parking/Balance
        [HttpGet]
        public JsonResult Balance()
        {
            var balance = new { balance = Parking.Instance.Balance };
            return Json(balance);
        }

    }
}
