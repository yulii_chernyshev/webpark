﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkLibrary
{
    public static class Settings
    {
        public static readonly int defaultBalance = 0;
        public static readonly int defaultPlaces = 10;
        public static readonly int timeToPay = 10;
        public static readonly double fine = 1.5;
        public static string LogPath { get; private set; } = "Transaction.log";
        public static readonly Dictionary<VehicleType, double> pricelist = new Dictionary<VehicleType, double>()
        {
            [VehicleType.Truck] = 4,
            [VehicleType.Car] = 3.5,
            [VehicleType.Bus] = 2,
            [VehicleType.Motorbike] = 1
        };


    }
}
