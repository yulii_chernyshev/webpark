﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkLibrary
{
    public class Transaction
    {
        private string vehNum;
        private double writeOff;
        public DateTime DateTime { get; private set; }

        public Transaction(DateTime dateTime, string vehNum, double writeOff)
        {
            DateTime = dateTime;
            VehNum = vehNum;
            WriteOff = writeOff;
        }

        public string VehNum
        {
            get { return vehNum; }
            private set
            {
                if (value.Length != 0)
                    vehNum = value;
                else
                    throw new Exception("Номер авто должен быть указан!");
            }
        }


        public double WriteOff
        {
            get { return writeOff; }
            private set
            {
                if (value >= 0)
                    writeOff = value;
                else
                    throw new Exception("Списанные средства не могут быть отрицательными!");
            }
        }

    }
}
