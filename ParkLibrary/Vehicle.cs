﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;

namespace ParkLibrary
{
    public enum VehicleType { Car, Truck, Bus, Motorbike };

    public class Vehicle
    {
        private double balance;
        private string VehicleID;
        private object locker = new object();
        Timer timer = new Timer();
        public readonly VehicleType VehicleType;

        public Vehicle(string _VehicleId, double _balance, VehicleType _vehicleType)
        {
            VehicleID = _VehicleId;
            balance = _balance;
            VehicleType = _vehicleType;
        }

        public string ShowID()
        {
            return VehicleID;
        }


        public double Balance
        {
            get
            {
                lock (locker)
                {
                    return balance;
                }
            }
            set
            {
                lock (locker)
                {
                    balance = value;
                }
            }
        }


        public string VehicleNum
        {
            get { return VehicleID; }
            private set
            {
                if (value.Length == 0)
                    throw new Exception("Номер авто должен быть указан!");
                else if (Parking.Instance.vehicles.Where(veh => veh.VehicleNum == value).Any())
                    throw new Exception("Номер авто не может повторяться!");
                else
                    VehicleID = value;
            }
        }

        public void AddToParking(Parking parking)
        {
            if (Settings.pricelist[VehicleType] > balance)
                Console.WriteLine("У вас не хватает денег для парковки!");
            else
            {
                parking.vehicles.Add(this);
                timer.Interval = Settings.timeToPay * 1000;
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
                Console.WriteLine("Автомобиль был успешно добавлен в паркинг!");
            }
        }


        public void RemoveFromParking(Parking parking)
        {
            if (balance < 0)
                Console.WriteLine($"Вы не можете покинуть паркинг! У вас есть долги! Пополните баланс на {Math.Abs(balance)}!");
            else
            {
                parking.vehicles.Remove(this);
                timer.Stop();
                Console.WriteLine("Автомобиль успешно покинул паркинг!");

            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            double cost = Settings.pricelist[this.VehicleType];
            if (balance < cost)
                cost *= Settings.fine;
            balance -= cost;
            Parking.Instance.Balance += cost;
            Transaction transaction = new Transaction(DateTime.Now, VehicleType.ToString(), cost);
            Parking.Instance.Transactions.Add(transaction);
        }


    }
}
